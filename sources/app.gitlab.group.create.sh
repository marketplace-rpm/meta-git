#!/usr/bin/env bash

(( ${EUID} == 0 )) &&
  { echo >&2 "This script should not be run as root!"; exit 1; }

# -------------------------------------------------------------------------------------------------------------------- #
# Get options.
# -------------------------------------------------------------------------------------------------------------------- #

curl=$( which curl )
api_ver="4"
sleep="2"

OPTIND=1

while getopts "t:n:v:g:h" opt; do
  case ${opt} in
    t)
      token="${OPTARG}"
      ;;
    n)
      group_name="${OPTARG}"; IFS=';' read -a group_name <<< "${group_name}"
      ;;
    v)
      group_visibility="${OPTARG}"
      ;;
    g)
      group_parent="${OPTARG}"
      ;;
    h|*)
      echo "-t [token] -n [group_name] -v [group_visibility] -g [group_parent]"
      exit 2
      ;;
    \?)
      echo "Invalid option: -${OPTARG}."
      exit 1
      ;;
    :)
      echo "Option -${OPTARG} requires an argument."
      exit 1
      ;;
  esac
done

shift $(( ${OPTIND} - 1 ))

(( ! ${#group_name[@]} )) || [[ -z "${parent}" ]] && exit 1

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

case ${group_visibility} in
  private)
    group_visibility="private"
    ;;
  internal)
    group_visibility="internal"
    ;;
  public)
    group_visibility="public"
    ;;
  *)
    exit 1
    ;;
esac

for i in "${group_name[@]}"; do
  group_url="$( echo ${i} | tr '[:upper:]' '[:lower:]' )"

  echo "" && echo "--- Open: ${i}"

  ${curl}                             \
  --header "PRIVATE-TOKEN: ${token}"  \
  --request POST                      \
  "https://gitlab.com/api/v${api_ver}/groups?name=${i}&path=${group_url}&visibility=${group_visibility}&parent_id=${group_parent}"

  echo "" && echo "--- Done: ${i}" && echo ""

  sleep ${sleep}
done

# -------------------------------------------------------------------------------------------------------------------- #
# Exit.
# -------------------------------------------------------------------------------------------------------------------- #

exit 0
