#!/usr/bin/env bash

(( ${EUID} == 0 )) &&
  { echo >&2 "This script should not be run as root!"; exit 1; }

# -------------------------------------------------------------------------------------------------------------------- #
# Get options.
# -------------------------------------------------------------------------------------------------------------------- #

curl=$( which curl )
api_ver="4"
sleep="2"

OPTIND=1

while getopts "t:i:f:h" opt; do
  case ${opt} in
    t)
      token="${OPTARG}"
      ;;
    i)
      project_id="${OPTARG}"; IFS=';' read -a project_id <<< "${project_id}"
      ;;
    f)
      upload_file="${OPTARG}"
      ;;
    h|*)
      echo "-t [token] -i [project_id] -f [upload_file]"
      exit 2
      ;;
    \?)
      echo "Invalid option: -${OPTARG}."
      exit 1
      ;;
    :)
      echo "Option -${OPTARG} requires an argument."
      exit 1
      ;;
  esac
done

shift $((${OPTIND} - 1))

[[ -z "${project_id}" ]] || [[ -z "${upload_file}" ]] && exit 1

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

echo "" && echo "--- Upload: ${project_id} < ${upload_file}"

${curl}                             \
--header "PRIVATE-TOKEN: ${token}"  \
--request POST                      \
--form "file=@${upload_file}"       \
"https://gitlab.com/api/v${api_ver}/projects/${project_id}/uploads"

echo "" && echo "--- Done: ${project_id} < ${upload_file}" && echo "" && sleep ${sleep}

# -------------------------------------------------------------------------------------------------------------------- #
# Exit.
# -------------------------------------------------------------------------------------------------------------------- #

exit 0
