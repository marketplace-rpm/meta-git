%global app                     git
%global d_bin                   %{_bindir}

Name:                           meta-git
Version:                        1.0.0
Release:                        5%{?dist}
Summary:                        META-package for install and configure Git
License:                        GPLv3

Source10:                       app.%{app}.sh
Source11:                       app.%{app}hub.project.create.sh
Source12:                       app.%{app}lab.group.create.sh
Source13:                       app.%{app}lab.group.remove.sh
Source14:                       app.%{app}lab.project.create.sh
Source15:                       app.%{app}lab.project.remove.sh
Source16:                       app.%{app}lab.project.services.packagist.create.sh
Source17:                       app.%{app}lab.project.upload.sh

Requires:                       git

%description
META-package for install and configure Git.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0755 %{SOURCE10} \
  %{buildroot}%{d_bin}/app.git.sh
%{__install} -Dp -m 0755 %{SOURCE11} \
  %{buildroot}%{d_bin}/app.github.project.create.sh
%{__install} -Dp -m 0755 %{SOURCE12} \
  %{buildroot}%{d_bin}/app.gitlab.group.create.sh
%{__install} -Dp -m 0755 %{SOURCE13} \
  %{buildroot}%{d_bin}/app.gitlab.group.remove.sh
%{__install} -Dp -m 0755 %{SOURCE14} \
  %{buildroot}%{d_bin}/app.gitlab.project.create.sh
%{__install} -Dp -m 0755 %{SOURCE15} \
  %{buildroot}%{d_bin}/app.gitlab.project.remove.sh
%{__install} -Dp -m 0755 %{SOURCE16} \
  %{buildroot}%{d_bin}/app.gitlab.project.services.packagist.create.sh
%{__install} -Dp -m 0755 %{SOURCE17} \
  %{buildroot}%{d_bin}/app.gitlab.project.upload.sh


%files
%{d_bin}/app.%{app}.sh
%{d_bin}/app.%{app}hub.project.create.sh
%{d_bin}/app.%{app}lab.group.create.sh
%{d_bin}/app.%{app}lab.group.remove.sh
%{d_bin}/app.%{app}lab.project.create.sh
%{d_bin}/app.%{app}lab.project.remove.sh
%{d_bin}/app.%{app}lab.project.services.packagist.create.sh
%{d_bin}/app.%{app}lab.project.upload.sh


%changelog
* Wed Jul 31 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-5
- UPD: SPEC-file.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-4
- UPD: SPEC-file.

* Wed Jul 10 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-3
- UPD: SH-files.

* Tue Jul 02 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-2
- UPD: SPEC-file.

* Fri Mar 22 2019 Kitsune Solar <kitsune.solar@gmail.com> - 1.0.0-1
- Initial build.
